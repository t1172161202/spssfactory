﻿namespace SpssFactory.Owns
{
    using SpssFactory.ViewModels;
    using SpssFactory.Models.Specs;
    using System;
    using SpssFactory.Owns;
    using CommonLib;
    using System.Collections.Generic;
    using SpssFactory.Models.Records;
    using SpssFactory.Helpers;
    using System.Data;

    /// <summary>
    /// グローバル変数・メソッド群
    /// </summary>
    public static class Globals
    {
        #region 08. Properties ---------------------------------------------------------------------

        /// <summary>
        /// 仕様書ファイル
        /// </summary>
        public static SpecFile Spec { get; set; } 

        /// <summary>
        /// メインウィンドウモデル
        /// </summary>
        public static MainWindowModel MainWM { get; set; }

        /// <summary>
        /// ホームビューモデル
        /// </summary>
        public static HomeViewModel HomeVM { get { return MainWM.Views.Find(v => v.Id == ViewId.Home).ViewModel as HomeViewModel; } }

        /// <summary>
        /// アイテムビューモデル
        /// </summary>
        public static SpecItemViewModel ItemVM { get { return HomeVM.Views.Find(v => v.Id == ViewId.SpecItem).ViewModel as SpecItemViewModel; } }

        #endregion

        #region 10. Methods ------------------------------------------------------------------------

        /// <summary>
        /// スナックバーメッセージを表示する
        /// </summary>
        /// <param name="message"></param>
        public static void SendToSnackbar(string message) => MainWM.Snackbar.MessageQueue.Enqueue(message);

        #endregion
    }
}
