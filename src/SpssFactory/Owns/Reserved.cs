﻿namespace SpssFactory.Owns
{
    public struct WindowStatus
    {
        public const bool Idle = false;
        public const bool Running = true;
    }

    public struct ExecutionResult
    {
        public const bool Failed = false;
        public const bool Succeeded = true;
    }

    public struct VisibleStatus
    {
        public const bool Invisible = false;
        public const bool Visible = true;
    }
}
