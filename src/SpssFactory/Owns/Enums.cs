﻿namespace SpssFactory.Owns
{
    using static Helpers.EnumHelper;

    /// <summary>
    /// ビュー識別子
    /// </summary>
    public enum ViewId
    {
        /// <summary>未設定</summary>
        [Optional("TBD")]
        TBD = 0,
        /// <summary>仕様ファイル選択</summary>
        [Optional("仕様ファイル選択")]
        Splash,
        /// <summary>ホーム</summary>
        [Optional("ホーム")]
        Home,
        /// <summary>デモ</summary>
        [Optional("デモ")]
        Demo,
        /// <summary>出力設定</summary>
        [Optional("出力設定")]
        SpecOrder,
        /// <summary>アイテム設定</summary>
        [Optional("アイテム設定")]
        SpecItem,
        /// <summary>ニュー設定</summary>
        [Optional("ニュー設定")]
        SpecLogic,
        /// <summary>分類設定</summary>
        [Optional("分類設定")]
        SpecClass,
    }


    /// <summary>
    /// ボタン種別
    /// </summary>
    /// <remarks>
    /// ログ用にオプション属性でボタン名を設定しておくこと
    /// </remarks>
    public enum ButtonKind
    {
        /// <summary>未設定</summary>
        [Optional("TBD")]
        TBD = 0,
        /// <summary>OK</summary>
        [Optional("OK")]
        Ok = 1,
        /// <summary>Cancel</summary>
        [Optional("Cancel")]
        Cancel = 2,
        [Optional("出力設定")]
        ToOrder,
        [Optional("アイテム設定")]
        ToItem,
        [Optional("ニュー設定")]
        ToLogic,
        [Optional("分類設定")]
        ToClass,
    }

    /// <summary>
    /// 仕様書ファイルワークシート種別
    /// </summary>
    /// <remarks>
    /// オプション値にはシート名を設定すること
    /// </remarks>
    public enum SpecSheetKind
    {
        /// <summary>システム設定シート</summary>
        [Optional("システム設定")]
        System = 0,
        /// <summary>出力（有無・順序）設定シート</summary>
        [Optional("出力設定")]
        Order = 1,
        /// <summary>アイテム設定シート</summary>
        [Optional("アイテム設定")]
        Item = 2,
        /// <summary>ニューアイテム作成ロジック設定シート</summary>
        [Optional("ニュー設定")]
        Logic = 3,
        /// <summary>アイテム分類設定シート</summary>
        [Optional("分類設定")]
        Class = 4,
    }

    /// <summary>
    /// レコードステータス種別
    /// </summary>
    public enum RecordStatusKind
    {
        [Optional("変更なし")]
        FIX = 0,

        // 更新
        [Optional("更新")]
        UPDATE = 1,

        // 削除
        [Optional("削除")]
        DELETE = 2,

        // 新規
        [Optional("新規")]
        NEW = 3,

        // エラー
        [Optional("エラー")]
        ERROR = 4,
    }

    /// <summary>
    /// アイテム型種別
    /// </summary>
    public enum TypeKind
    {
        [Optional("S")]
        S = 0,
        [Optional("M")]
        M,
        [Optional("C")]
        C,
        [Optional("R")]
        R,
        [Optional("W")]
        W,
    }
}
