﻿namespace SpssFactory.WpfUtils
{
    using System;
    using System.Globalization;
    using System.Windows.Data;

    /// <summary>
    /// 複引数コンバーター規定クラス
    /// </summary>
    /// <remarks>
    /// 継承先でメソッド ConvertBack を省略する為の規定クラス
    /// </remarks>
    public class MultiValueConverterBASE : IMultiValueConverter
    {
        /// <summary>
        /// ソース値をバインディングターゲットの値に変換する
        /// </summary>
        /// <param name="values">マルチバインディングのソース バインディングが生成する値の配列</param>
        /// <param name="targetType">バインディングターゲットプロパティの型</param>
        /// <param name="parameter">使用するコンバーターパラメーター</param>
        /// <param name="culture">コンバーターで使用するカルチャ</param>
        /// <returns>変換された値</returns>
        public virtual object Convert(
            object[] values,
            Type targetType,
            object parameter,
            CultureInfo culture) => values.Clone();

        /// <summary>
        /// バインディングターゲットの値をバインディングソースの値に変換する
        /// </summary>
        /// <param name="value">バインディングターゲットによって生成される値</param>
        /// <param name="targetTypes">変換対象の型の配列</param>
        /// <param name="parameter">使用するコンバーターパラメーター</param>
        /// <param name="culture">コンバーターで使用するカルチャ</param>
        /// <returns>ターゲット値からソース値に変換された値の配列</returns>
        public virtual object[] ConvertBack(
            object value,
            Type[] targetTypes,
            object parameter,
            CultureInfo culture) => throw new NotImplementedException();
    }
}
