﻿namespace SpssFactory.WpfUtils
{
    using System;
    using System.Globalization;
    using System.Windows.Data;

    /// <summary>
    /// 単引数コンバーター基底クラス
    /// </summary>
    /// <remarks>
    /// 継承先で ConvertBack メソッドを省略する為の規定クラス
    /// </remarks>
    public class SingleValueConverterBASE : IValueConverter
    {
        /// <summary>
        /// オブジェクトを表すバインディングターゲットの値に変換する
        /// </summary>
        /// <param name="value">バインディング ソースによって生成された値</param>
        /// <param name="targetType">バインディングターゲットプロパティの型</param>
        /// <param name="parameter">使用するコンバーターパラメーター</param>
        /// <param name="culture">コンバーターで使用するカルチャ</param>
        /// <returns>変換された値</returns>
        public virtual object Convert(
            object value,
            Type targetType,
            object parameter,
            CultureInfo culture) => throw new NotImplementedException();

        /// <summary>
        /// バインディングターゲットの値をバインディングソースの値に変換する
        /// </summary>
        /// <param name="value">バインディングターゲットによって生成される値</param>
        /// <param name="targetType">変換対象の型</param>
        /// <param name="parameter">使用するコンバーターパラメーター</param>
        /// <param name="culture">コンバーターで使用するカルチャ</param>
        /// <returns>ターゲット値からソース値に変換された値</returns>
        public object ConvertBack(
            object value,
            Type targetType,
            object parameter,
            CultureInfo culture) => throw new NotImplementedException();
    }
}
