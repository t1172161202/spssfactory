﻿namespace SpssFactory.WpfUtils
{
    using System;
    using System.Windows.Input;

    /// <summary>
    /// メソッドをコマンドとして登録して使用するためのクラス
    /// </summary>
    public class CommandImplementation : ICommand
    {
        #region 01. Fields -------------------------------------------------------------------------

        /// <summary>
        /// コマンド実行式
        /// </summary>
        private readonly Action<object> execute;

        /// <summary>
        /// コマンド実行可否判定式
        /// </summary>
        private readonly Func<object, bool> canExecute;

        #endregion

        #region 02. Constructors -------------------------------------------------------------------

        /// <summary>
        /// コマンドを登録する
        /// </summary>
        /// <param name="execute">コマンドとして登録するメソッド</param>
        public CommandImplementation(Action<object> execute) : this(execute, null) { /* empty */ }

        /// <summary>
        /// コマンドとコマンド実行可否判定式を登録する
        /// </summary>
        /// <param name="execute">コマンド実行式</param>
        /// <param name="canExecute">コマンド実行可否判定式</param>
        public CommandImplementation(Action<object> execute, Func<object, bool> canExecute)
        {
            this.execute = execute ?? throw new ArgumentNullException(nameof(execute));

            // コマンド実行可否判定式が空の場合は trueを返却
            this.canExecute = canExecute ?? (x => true);
        }

        #endregion

        #region 05. Events -------------------------------------------------------------------------

        /// <summary>
        /// コマンド実行可否変更イベント
        /// </summary>
        public event EventHandler CanExecuteChanged
        {
            add
            {
                CommandManager.RequerySuggested += value;
            }
            remove
            {
                CommandManager.RequerySuggested -= value;
            }
        }

        #endregion

        #region 10. Methods ------------------------------------------------------------------------

        /// <summary>
        /// コマンド実行可否判定結果を返却する
        /// </summary>
        /// <param name="parameter">コマンドパラメータ</param>
        /// <returns>実行可否</returns>
        public bool CanExecute(object parameter) => canExecute(parameter);

        /// <summary>
        /// コマンドを実行する
        /// </summary>
        /// <param name="parameter">コマンドパラメータ</param>
        public void Execute(object parameter) => execute(parameter);

        /// <summary>
        /// コマンド実行可否判定を強制実行する
        /// </summary>
        public void Refresh() => CommandManager.InvalidateRequerySuggested();

        #endregion
    }
}
