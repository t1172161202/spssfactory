﻿namespace SpssFactory.WpfUtils
{
    using MahApps.Metro.Controls;
    using Microsoft.Xaml.Behaviors;
    using System.ComponentModel;

    public class WindowClosingBehavior : Behavior<MetroWindow>
    {
        protected override void OnAttached()
        {
            base.OnAttached();
            AssociatedObject.Closing += OnClosing;
        }

        protected override void OnDetaching()
        {
            base.OnDetaching();
            AssociatedObject.Closing -= OnClosing;
        }

        /// <summary>
        /// メトロウィンドウクロージングイベントハンドラ
        /// </summary>
        /// <param name="sender">イベント発生元オブジェクト</param>
        /// <param name="e">イベント詳細</param>
        private void OnClosing(object sender, CancelEventArgs e)
        {
            var window = sender as MetroWindow;

            // ViewModelが IClosingインターフェイスを実装していたらメソッドを実行する
            if (window.DataContext is IClosing)
            {
                e.Cancel = (window.DataContext as IClosing).Closing();
            }
        }
    }
}
