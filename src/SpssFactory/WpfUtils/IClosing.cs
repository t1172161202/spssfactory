﻿namespace SpssFactory.WpfUtils
{
    public interface IClosing
    {
        /// <summary>
        /// クロージングイベントハンドラ
        /// </summary>
        /// <returns>クロージングイベントをキャンセルするか？</returns>
        bool Closing();
    }
}
