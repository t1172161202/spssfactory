﻿namespace SpssFactory.WpfUtils
{
    using SpssFactory.Owns;
    using SpssFactory.ViewModels;
    using System;
    using System.Globalization;

    /// <summary>
    /// ビュー識別子コンバーター
    /// </summary>
    public class ViewIdConverter : SingleValueConverterBASE
    {
        /// <summary>
        /// オブジェクトを表すバインディングターゲットの値に変換する
        /// </summary>
        /// <param name="value">オブジェクト</param>
        /// <param name="targetType">バインディングターゲットプロパティの型</param>
        /// <param name="parameter">使用するコンバーターパラメーター</param>
        /// <param name="culture">コンバーターで使用するカルチャ</param>
        /// <returns>変換された値</returns>
        public override object Convert(
            object value,
            Type targetType,
            object parameter,
            CultureInfo culture)
        {
            var index = FindIndex(Globals.MainWM);
            if (index == -1)
            {
                throw new NotImplementedException();
            }
            return index;
            
            // 再帰処理
            int FindIndex(ViewModelBASE vm)
            {
                var index = vm.Views?.FindIndex(view => view.Id == (ViewId)value);
                if (index == -1)
                {
                    foreach (var v in vm.Views)
                    {
                        index = FindIndex(v.ViewModel);
                        if (index != -1)
                        {
                            return (int)index;
                        }
                    }
                }
                return index ?? -1;
            }
        }    
    }
}
