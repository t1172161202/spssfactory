﻿namespace SpssFactory.Views
{
    using MahApps.Metro.Controls;
    using SpssFactory.Owns;
    using SpssFactory.ViewModels;

    /// <summary>
    /// メインウィンドウ
    /// </summary>
    public partial class MainWindow : MetroWindow
    {
        #region 02. Constructors -------------------------------------------------------------------

        /// <summary>
        /// ビューモデルとビューの関連付けを行う
        /// </summary>
        public MainWindow()
        {
            InitializeComponent();
            Globals.MainWM = new MainWindowModel()
            {
                ViewTransitioner = transitioner,
                Snackbar = snackbar,
            };
            Globals.MainWM.SetupSlides();
            DataContext = Globals.MainWM;
        }

        #endregion
    }
}
