﻿namespace SpssFactory.Views
{
    using MaterialDesignThemes.Wpf.Transitions;
    using SpssFactory.Owns;
    using SpssFactory.ViewModels;

    /// <summary>
    /// ビュー基底クラス
    /// </summary>
    public class ViewBASE : TransitionerSlide
    {
        #region 02. Constructors -------------------------------------------------------------------

        /// <summary>
        /// コンストラクタ
        /// </summary>
        public ViewBASE() { }

        /// <summary>
        /// コンストラクタ
        /// </summary>
        /// <param name="id">ビュー識別子</param>
        public ViewBASE(ViewId id) : this() => Id = id;

        #endregion

        #region 08. Properties ---------------------------------------------------------------------

        /// <summary>
        /// ビュー識別子
        /// </summary>
        public ViewId Id { get; set; } = ViewId.TBD;

        /// <summary>
        /// ビューモデル
        /// </summary>
        public ViewModelBASE ViewModel { get; set; }

        #endregion
    }
}
