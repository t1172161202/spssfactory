﻿namespace SpssFactory.Views
{
    using SpssFactory.Owns;
    using SpssFactory.ViewModels;

    /// <summary>
    /// ホームビュー
    /// </summary>
    public partial class SpecOrderView : ViewBASE
    {
        #region 02. Constructors -------------------------------------------------------------------

        /// <summary>
        /// コンストラクタ
        /// </summary>
        /// <param name="viewModel">ビューモデル</param>
        public SpecOrderView(ViewModelBASE viewModel) : base(viewModel.ViewId)
        {
            InitializeComponent();
            DataContext = ViewModel = viewModel;
        }

        #endregion
    }
}
