﻿namespace SpssFactory.Views
{
    using SpssFactory.ViewModels;

    /// <summary>
    /// スプラッシュビュー
    /// </summary>
    public partial class SplashView : ViewBASE
    {
        #region 02. Constructors -------------------------------------------------------------------

        /// <summary>
        /// コンストラクタ
        /// </summary>
        /// <param name="viewModel">ビューモデル</param>
        public SplashView(ViewModelBASE viewModel) : base(viewModel.ViewId)
        {
            InitializeComponent();
            DataContext = ViewModel = viewModel;
        }

        #endregion
    }
}
