﻿namespace SpssFactory.Views
{
    using SpssFactory.Owns;
    using SpssFactory.ViewModels;

    /// <summary>
    /// ホームビュー
    /// </summary>
    public partial class SpecItemView : ViewBASE
    {
        #region 02. Constructors -------------------------------------------------------------------

        /// <summary>
        /// コンストラクタ
        /// </summary>
        /// <param name="viewModel">ビューモデル</param>
        public SpecItemView(ViewModelBASE viewModel) : base(viewModel.ViewId)
        {
            InitializeComponent();
            DataContext = ViewModel = viewModel;
        }

        #endregion
    }
}
