﻿namespace SpssFactory.Views
{
    using MaterialDesignThemes.Wpf;
    using Reactive.Bindings;
    using SpssFactory.Helpers;
    using SpssFactory.Owns;
    using System.Data;
    using System.Linq;
    using System.Reactive.Linq;
    using System.Windows;
    using System.Windows.Controls;

    /// <summary>
    /// カスタムボタン
    /// </summary>
    public partial class CustomButton : UserControl
    {
        #region 02. Constructors -------------------------------------------------------------------        

        /// <summary>
        /// このコントロールを配置した親コントロールのデータコンテクストとバインド
        /// </summary>
        public CustomButton()
        {
            InitializeComponent();
            (Content as FrameworkElement).DataContext = this;

            // 連鎖更新プロパティを登録
            IconVisibility =
                Icon.Select(x =>
                    x != PackIconKind.None
                        ? Visibility.Visible
                        : Visibility.Collapsed
                ).ToReactiveProperty();
        }

        #endregion
  
        #region 08. Properties (Parameters)---------------------------------------------------------

        /// <summary>
        /// ボタン種別パラメータ
        /// </summary>
        /// <remarks>
        /// 配置先 .xaml上で本パラメータを設定して下さい
        /// </remarks>
        public ButtonKind ButtonKindParameter
        {
            get { return ButtonKind.Value; }
            set
            { 
                ButtonKind.Value = value;
                if (string.IsNullOrEmpty(Label.Value))
                {
                    Label.Value = ButtonKind.Value.GetValue();
                }
            }
        }

        /// <summary>
        /// ラベルパラメータ
        /// </summary>
        /// <remarks>
        /// 配置先 .xaml上で本パラメータを設定して下さい
        /// </remarks>
        public string LabelParameter
        { 
            get { return Label.Value; }
            set { Label.Value = value; }
        }

        /// <summary>
        /// アイコンパラメータ
        /// </summary>
        /// <remarks>
        /// 配置先 .xaml上で本パラメータを設定して下さい
        /// </remarks>
        public PackIconKind IconParameter { set { Icon.Value = value; } }

        #endregion
        #region 08. Properties --------------------------------------------------------------------- 

        /// <summary>
        /// アイコン表示ステータス
        /// </summary>
        public IReactiveProperty<Visibility> IconVisibility { get; set; }

        /// <summary>
        /// ボタン種別
        /// </summary>
        public ReactiveProperty<ButtonKind> ButtonKind { get; set; } = new ReactiveProperty<ButtonKind>(Owns.ButtonKind.TBD);

        /// <summary>
        /// ラベル
        /// </summary>
        public ReactiveProperty<string> Label { get; set; } = new ReactiveProperty<string>(string.Empty);

        /// <summary>
        /// アイコン
        /// </summary>
        public ReactiveProperty<PackIconKind> Icon { get; set; } = new ReactiveProperty<PackIconKind>(PackIconKind.None);

        #endregion
    }
}
