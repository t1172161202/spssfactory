﻿namespace SpssFactory.Views
{
    using SpssFactory.ViewModels;

    /// <summary>
    /// ホームビュー
    /// </summary>
    public partial class HomeView : ViewBASE
    {
        #region 02. Constructors -------------------------------------------------------------------

        /// <summary>
        /// ビューモデルとビューの関連付けを行う
        /// </summary>
        /// <param name="viewModel">ビューモデル</param>
        public HomeView(ViewModelBASE viewModel) : base(viewModel.ViewId)
        {
            InitializeComponent();
            (viewModel as HomeViewModel).ViewTransitioner = transitioner;
            (viewModel as HomeViewModel).SetupSlides();
            DataContext = ViewModel = viewModel;
        }

        #endregion
    }
}
