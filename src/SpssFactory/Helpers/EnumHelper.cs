﻿namespace SpssFactory.Helpers
{
    using System;
    using System.Linq;

    /// <summary>
    /// 列挙型ヘルパークラス
    /// </summary>
    public static class EnumHelper
    {
        #region 10. Methods ------------------------------------------------------------------------

        /// <summary>
        /// オプション値を取得する
        /// </summary>
        /// <param name="value">対象列挙型</param>
        /// <returns>オプション値</returns>
        /// <example>
        /// var val = EnumHelper.GetValue(enum);
        /// var val = enum.GetValue()
        /// </example>
        public static string GetValue(this Enum value) => value.GetAttribute<OptionalAttribute>()?.Value ?? value.ToString();

        /// <summary>
        /// [拡張メソッド] 指定オプション値を取得する
        /// </summary>
        /// <typeparam name="T">属性型</typeparam>
        /// <param name="value">対象列挙型</param>
        /// <returns>属性値</returns>
        private static T GetAttribute<T>(this Enum value) where T : Attribute
        {
            // リフレクションを用いて列挙体の型から情報を取得する
            var fieldInfo = value.GetType().GetField(value.ToString());

            // 指定した属性を配列で取得する
            var attributes = fieldInfo.GetCustomAttributes(typeof(T), false).Cast<T>();

            // 属性がなかった場合は nullを返す
            if ((attributes?.Count() ?? 0) <= 0) return null;

            // 同じ属性が複数含まれている場合は先頭を返す
            return attributes.First();
        }

        #endregion

        #region 12. Classes ------------------------------------------------------------------------

        /// <summary>
        /// オプション値型クラス
        /// </summary>
        [AttributeUsage(AttributeTargets.Field, Inherited = false, AllowMultiple = false)]
        public sealed class OptionalAttribute : Attribute
        {
            #region 02. Constructors -------------------------------------------------------------------

            /// <summary>
            /// コンストラクタ
            /// </summary>
            /// <param name="value">オプション値</param>
            public OptionalAttribute(string value) => Value = value;

            #endregion

            #region 08. Properties ---------------------------------------------------------------------

            /// <summary>
            /// オプション値
            /// </summary>
            public string Value { get; private set; }
            #endregion
        }

        #endregion
    }
}
