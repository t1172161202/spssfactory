﻿namespace SpssFactory.Helpers
{
    using Microsoft.Win32;
    using System.Collections.Generic;
    using System.Windows;

    public static class WpfHelper
    {
        #region 10. Methods ------------------------------------------------------------------------

        /// <summary>
        /// ファイル選択ダイアログを表示し、ユーザ操作結果を返却する
        /// </summary>
        /// <param name="dialog">ダイアログ</param>
        /// <returns>
        /// ユーザ操作結果
        /// </returns>
        /// <example>
        /// Filter = "Excel ファイル (*.xlsx)|*.xlsx"
        /// </example>
        public static FileSelectDialogResult ShowFileSelectDialog(OpenFileDialog dialog)
        {
            // ダイアログでファイルが選択されなかった場合はここでおしまい
            if (!dialog.ShowDialog(Application.Current.MainWindow) ?? true)
            {
                return new FileSelectDialogResult();
            }
            else
            {
                return new FileSelectDialogResult()
                {
                    HasSelected = true,
                    SelectedPaths = new List<string>(dialog.FileNames),
                };
            }
        }

        #endregion

        #region 12. Classes ------------------------------------------------------------------------

        /// <summary>
        /// ファイル選択ダイアログ操作結果
        /// </summary>
        public class FileSelectDialogResult
        {
            /// <summary>
            /// コンストラクタ
            /// </summary>
            public FileSelectDialogResult() { }

            /// <summary>
            /// ファイルが選択されたか？
            /// </summary>
            public bool HasSelected { get; set; } = false;

            /// <summary>
            /// 選択されたファイルパス
            /// </summary>
            public List<string> SelectedPaths { get; set; } = new List<string>();
        }

        #endregion
    }
}
