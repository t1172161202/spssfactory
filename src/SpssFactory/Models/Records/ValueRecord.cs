﻿namespace SpssFactory.Models.Records
{
    using CommonLib;
    using SpreadsheetGear;
    using System;
    using System.Data;
    using CI = Specs.ItemSpecSheet.ColumnIndex;

    /// <summary>
    /// 値レコード
    /// </summary>
    [Serializable]
    public class ValueRecord : RecordBASE
    {
        #region 02. Constructors -------------------------------------------------------------------

        /// <summary>
        /// コンストラクタ
        /// </summary>
        public ValueRecord() { }

        /// <summary>
        /// コンストラクタ
        /// </summary>
        /// <param name="variableIndex">親変数インデックス</param>
        /// <param name="valueIndex">レコードインデックス</param>
        /// <param name="row">レコード行データ</param>
        public ValueRecord(int variableIndex, int valueIndex, IRange row)
            : base(valueIndex)
        {
            ParentIndex = variableIndex;
            Index = valueIndex;
            DeleteFlag = !string.IsNullOrWhiteSpace(row.GetValue(CI.VarDelete));
            OutValue = ConvertHelper.ToString(row.GetValue(CI.OutValue), true);            
            Label = ConvertHelper.ToString(row.GetValue(CI.ValLabel));
            LowerLimit = ConvertHelper.ToInt(row.GetValue(CI.LowerLimit));
            UpperLimit = ConvertHelper.ToInt(row.GetValue(CI.UpperLimit));
            InValue = ConvertHelper.ToString(row.GetValue(CI.InValue), true);
            InLogic = ConvertHelper.ToString(row.GetValue(CI.InLogic));

            Snapshot = this.Clone();
        }

#endregion

        #region 08. Properties ---------------------------------------------------------------------

        /// <summary>
        /// 親変数インデックス
        /// </summary>
        public int ParentIndex { get; set; }

        /// <summary>
        /// 削除フラグ
        /// </summary>
        public bool DeleteFlag { get; set; }

        /// <summary>
        /// 出力値
        /// </summary>
        public string OutValue { get; set; }

        /// <summary>
        /// ラベル
        /// </summary>
        public string Label { get; set; }

        /// <summary>
        /// 中央値下限値
        /// </summary>
        public double? LowerLimit { get; set; }

        /// <summary>
        /// 中央値上限値
        /// </summary>
        public double? UpperLimit { get; set; }

        /// <summary>
        /// 入力値
        /// </summary>
        public string InValue { get; set; }

        /// <summary>
        /// （入力値を表す）ロジック
        /// </summary>
        public string InLogic { get; set; }

        #endregion
    }
}
