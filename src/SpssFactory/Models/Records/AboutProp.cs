﻿namespace SpssFactory.Models.Records
{
    using System;

    /// <summary>
    /// プロパティ情報クラス
    /// </summary>
    [Serializable]
    public class AboutProp
    {
        #region 02. Constructors -------------------------------------------------------------------

        /// <summary>
        /// プロパティ情報を登録する
        /// </summary>
        /// <param name="name">プロパティ名</param>
        /// <param name="label">プロパティラベル</param>
        /// <param name="canEmpty">空白またはヌルを許可するか？</param>
        /// <param name="canDuplicate">他レコードとの重複を許可するか？</param>
        public AboutProp(string name, string label, bool canEmpty, bool canDuplicate)
        {
            Name = name;
            Label = label;
            CanEmpty = canEmpty;
            CanDuplicate = canDuplicate;
        }

        #endregion

        #region 08. Properties ---------------------------------------------------------------------

        /// <summary>
        /// プロパティ名
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// プロパティラベル
        /// </summary>
        public string Label { get; set; }

        /// <summary>
        /// 空白またはヌルを許可するか？
        /// </summary>
        public bool CanEmpty { get; set; } = false;

        /// <summary>
        /// 他レコードとの重複を許可するか？
        /// </summary>
        public bool CanDuplicate { get; set; } = false;

        #endregion
    }
}
