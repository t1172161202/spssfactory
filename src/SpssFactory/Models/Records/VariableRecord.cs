﻿namespace SpssFactory.Models.Records
{
    using CommonLib;
    using SpreadsheetGear;
    using System;
    using System.Data;
    using CI = Specs.ItemSpecSheet.ColumnIndex;

    /// <summary>
    /// 変数レコード
    /// </summary>
    [Serializable]
    public class VariableRecord : RecordBASE
    {
        #region 01. Fields -------------------------------------------------------------------------        
        #endregion

        #region 02. Constructors -------------------------------------------------------------------

        /// <summary>
        /// コンストラクタ
        /// </summary>
        public VariableRecord() { }

        /// <summary>
        /// コンストラクタ
        /// </summary>
        /// <param name="index">レコードインデックス</param>
        /// <param name="row">レコード行データ</param>
        public VariableRecord(int index, IRange row)
            : base(index)
        {
            Index = index;
            DeleteFlag = !string.IsNullOrWhiteSpace(row.GetValue(CI.VarDelete));
            InVarName = ConvertHelper.ToString(row.GetValue(CI.InVarName), true);
            OutVarName = ConvertHelper.ToString(row.GetValue(CI.OutVarName), true);
            VarClass = ConvertHelper.ToString(row.GetValue(CI.Class), true);
            VarType = ConvertHelper.ToString(row.GetValue(CI.Type), true);
            Width = ConvertHelper.ToInt(row.GetValue(CI.Width));
            DecDigit = ConvertHelper.ToInt(row.GetValue(CI.DecDigit));
            Label = ConvertHelper.ToString(row.GetValue(CI.VarLabel));
            Snapshot = this.Clone();
        }

        #endregion

        #region 08. Properties ---------------------------------------------------------------------

        /// <summary>
        /// 削除フラグ
        /// </summary>
        public bool DeleteFlag { get; set; }

        /// <summary>
        /// 入力変数名称
        /// </summary>
        public string InVarName { get; set; }

        /// <summary>
        /// 出力変数名称
        /// </summary>
        public string OutVarName { get; set; }

        /// <summary>
        /// 分類
        /// </summary>
        public string VarClass { get; set; }

        /// <summary>
        /// 型
        /// </summary>
        public string VarType { get; set; }

        /// <summary>
        /// 幅
        /// </summary>
        public int? Width { get; set; }

        /// <summary>
        /// 小数桁数
        /// </summary>
        public int? DecDigit { get; set; }

        /// <summary>
        /// ラベル
        /// </summary>
        public string Label { get; set; }

        #endregion
    }
}
