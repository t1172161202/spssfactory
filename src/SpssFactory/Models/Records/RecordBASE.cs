﻿namespace SpssFactory.Models.Records
{
    using CommonLib;
    using SpssFactory.Owns;
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.Runtime.CompilerServices;

    /// <summary>
    /// レコード管理型規定クラス
    /// </summary>
    [Serializable]
    public class RecordBASE : ObservableObject
    {
        #region 01. Fields -------------------------------------------------------------------------

        /// <summary>
        /// レコードステータス
        /// </summary>
        /// <see cref="Status" />
        [NonSerialized]
        private RecordStatusKind status = RecordStatusKind.FIX;

        /// <summary>
        /// 設定エラーメッセージリスト
        /// </summary>
        /// <see cref="ErrorMessages" />
        [NonSerialized]
        private List<string> errorMessages = new List<string>();

        /// <summary>
        /// 変更前設定
        /// </summary>
        /// <see cref="Snapshot" />
        [NonSerialized]
        private RecordBASE saved = null;

        private int order = int.MinValue;

        #endregion

        #region 02. Constructors -------------------------------------------------------------------

        /// <summary>
        /// コンストラクタ
        /// </summary>
        public RecordBASE() { }

        /// <summary>
        /// コンストラクタ
        /// </summary>
        /// <param name="index">レコードインデックス</param>
        public RecordBASE(int index)
        {
            Index = index;
            Order = index + 1;
        }

        #endregion

        #region 08. Properties ---------------------------------------------------------------------

        /// <summary>
        /// レコードステータス
        /// </summary>
        public RecordStatusKind Status
        {
            get { return status; }
            set { SetProperty(ref status, value); }
        }

        /// <summary>
        /// 設定エラーメッセージリスト
        /// </summary>
        public List<string> ErrorMessages
        {
            get { return errorMessages; }
            set { SetProperty(ref errorMessages, value); }
        }

        /// <summary>
        /// 編集前設定（変更前設定）
        /// </summary>
        public RecordBASE Snapshot
        {
            get { return saved; }
            set { saved = value; }
        }

        /// <summary>
        /// インデックス
        /// </summary>
        public int Index { get; set; } = int.MinValue;

        /// <summary>
        /// 表示順序
        /// </summary>
        public int Order
        {
            get { return order; }
            set { SetProperty(ref order, value); }
        }

        /// <summary>
        /// 編集可能プロパティリスト
        /// </summary>
        protected virtual List<AboutProp> EditableProps { get; set; } = new List<AboutProp>();

        #endregion

        #region 10. Methods ------------------------------------------------------------------------

        /// <summary>
        /// NonSerialized属性フィールドを初期化する
        /// </summary>
        /// <param name="clone">編集前設定</param>
        /// <remarks>
        /// Cloneメソッドでは生成されないため、本メソッドで別途生成する必要があります
        /// </remarks>
        public void Initialize(RecordBASE clone)
        {
            status = RecordStatusKind.FIX;
            errorMessages = new List<string>();
            Snapshot = clone;
        }

        /// <summary>
        /// 更新有無を取得する
        /// </summary>
        /// <param name="updatesOrderOnly">表示順序のみ更新有無をチェックするか？</param>
        /// <returns>更新有無</returns>
        public bool Updates(bool updatesOrderOnly = false)
        {
            // 表示順序のみ更新有無をチェックする場合
            if (updatesOrderOnly)
            {
                return order != saved.order && status != RecordStatusKind.NEW;
            }
            else
            {
                return status != RecordStatusKind.FIX;
            }
        }

        /// <summary>
        /// プロパティ値を更新する
        /// </summary>
        /// <typeparam name="T">オブジェクト型</typeparam>
        /// <param name="storage">値を保存しているフィールド</param>
        /// <param name="value">更新に用いる値</param>
        /// <param name="propertyName">プロパティ名</param>
        /// <returns>更新成否</returns>
        protected new bool SetProperty<T>(ref T storage, T value, [CallerMemberName] string propertyName = null)
        {
            if (Equals(storage, value))
            {
                // 更新失敗の意で false を返却しておく
                return false;
            }

            storage = value;
            OnPropertyChanged(propertyName);
            UpdateStatus();

            // 更新成功の意で true を返却しておく
            return true;
        }

        /// <summary>
        /// 編集可能プロパティの設定を検証し、エラーメッセージと・変更有無ステータスフラグを更新する
        /// </summary>
        /// <param name="saved">変更前設定</param>
        protected virtual void ValidateEditableProps(RecordBASE saved) { }
        // TODO: 最終的には　↓　こっちにするよ
        // throw new NotImplementedException();

        /// <summary>
        /// レコードステータスを更新する
        /// </summary>
        private void UpdateStatus()
        {
            // レコード生成直後は変更前設定がクローン生成されていない（＝変更なし）ので処理を抜ける
            // 削除指定された場合も処理を抜ける
            if (saved == null || Status == RecordStatusKind.DELETE)
            {
                return;
            }

            // 編集可能プロパティの設定を検証し、エラーメッセージと・変更有無ステータスフラグを更新する
            ValidateEditableProps(saved);


            // 新規追加レコードの場合はステータスは更新しない
            if (Status == RecordStatusKind.NEW)
            {
                return;
            }

            // ステータスを更新する
            Status = ChangeExists
                ? RecordStatusKind.UPDATE
                : RecordStatusKind.FIX;
        }

        #endregion
    }
}
