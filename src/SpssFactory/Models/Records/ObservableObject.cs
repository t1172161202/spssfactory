﻿namespace SpssFactory.Models.Records
{
    using System;
    using System.ComponentModel;
    using System.Runtime.CompilerServices;

    /// <summary>
    /// 値監視型オブジェクトクラス
    /// </summary>
    [Serializable]
    public class ObservableObject : INotifyPropertyChanged
    {
        #region 05. Events -------------------------------------------------------------------------

        /// <summary>
        /// プロパティ値更新イベント
        /// </summary>
        public event PropertyChangedEventHandler PropertyChanged;

        #endregion

        #region 08. Properties ---------------------------------------------------------------------

        /// <summary>
        /// 変更有無ステータスフラグ
        /// </summary>
        protected bool ChangeExists { get; set; } = false;

        /// <summary>
        /// エラー有無ステータスフラグ
        /// </summary>
        protected bool ErrorExists { get; set; } = false;

        #endregion

        #region 10. Methods ------------------------------------------------------------------------

        /// <summary>
        /// プロパティ値更新イベントを呼び出す
        /// </summary>
        /// <param name="propertyName">プロパティ名</param>
        protected void OnPropertyChanged([CallerMemberName] string propertyName = null) =>
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));

        /// <summary>
        /// プロパティ値を更新する
        /// </summary>
        /// <typeparam name="T">オブジェクト型</typeparam>
        /// <param name="storage">値を保存しているフィールド</param>
        /// <param name="value">更新に用いる値</param>
        /// <param name="propertyName">プロパティ名</param>
        /// <returns>更新成否</returns>
        protected bool SetProperty<T>(ref T storage, T value, [CallerMemberName] string propertyName = null)
        {
            if (Equals(storage, value))
            {
                // 更新失敗の意で false を返却しておく
                return false;
            }

            storage = value;
            OnPropertyChanged(propertyName);

            // 更新成功の意で true を返却しておく
            return true;
        }

        #endregion
    }
}
