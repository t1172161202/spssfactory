﻿namespace SpssFactory.Models.Specs
{
    using SpssFactory.Owns;

    /// <summary>
    /// クラス設定シート仕様
    /// </summary>
    public class SpecClassSheet : SpecSheetBASE
    {
        #region 01. Fields -------------------------------------------------------------------------

        /// <summary>
        /// シート種別
        /// </summary>
        private static readonly SpecSheetKind kind = SpecSheetKind.Class;

        #endregion

        #region 02. Constructors -------------------------------------------------------------------

        /// <summary>
        /// 基底クラスにシート種別を登録する
        /// </summary>
        public SpecClassSheet() : base(kind) { }

        #endregion
    }
}
