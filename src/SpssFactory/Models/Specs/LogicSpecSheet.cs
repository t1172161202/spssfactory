﻿namespace SpssFactory.Models.Specs
{
    using SpssFactory.Owns;

    /// <summary>
    /// ニューアイテム作成ロジック設定シート仕様
    /// </summary>
    public class LogicSpecSheet : SpecSheetBASE
    {
        #region 01. Fields -------------------------------------------------------------------------

        /// <summary>
        /// シート種別
        /// </summary>
        private static readonly SpecSheetKind kind = SpecSheetKind.Logic;

        #endregion

        #region 02. Constructors -------------------------------------------------------------------

        /// <summary>
        /// 基底クラスにシート種別を登録する
        /// </summary>
        public LogicSpecSheet() : base(kind) { }

        #endregion
    }
}

