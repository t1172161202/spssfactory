﻿namespace SpssFactory.Models.Specs
{
    using System;
    using SpssFactory.Owns;
    using CommonLib;
    using System.Collections.Generic;
    using SpssFactory.Models.Records;
    using SpssFactory.Helpers;
    using SpreadsheetGear;

    /// <summary>
    /// アイテム設定シート仕様
    /// </summary>
    /// <remarks>
    /// アイテム設定＝変数設定＋値設定
    /// </remarks>
    public class ItemSpecSheet : SpecSheetBASE
    {
        #region 01. Fields -------------------------------------------------------------------------

        /// <summary>
        /// シート種別
        /// </summary>
        private static readonly SpecSheetKind kind = SpecSheetKind.Item;

        #endregion

        #region 02. Constructors -------------------------------------------------------------------

        /// <summary>
        /// 基底クラスにシート種別を登録する
        /// </summary>
        public ItemSpecSheet() : base(kind) { }

        #endregion

        #region 08. Properties ---------------------------------------------------------------------        
        
        /// <summary>
        /// 変数リスト
        /// </summary>
        public static List<VariableRecord> Variables { get; set; } = new List<VariableRecord>();

        /// <summary>
        /// 値リスト
        /// </summary>
        public static List<ValueRecord> Values { get; set; } = new List<ValueRecord>();

        #endregion

        #region 10. Methods ------------------------------------------------------------------------

        /// <summary>
        /// 仕様を読み込む
        /// </summary>
        /// <returns>実行成否</returns>
        public override bool Read()
        {
            Variables.Clear();
            Values.Clear();
            try
            {
                var endRowIndex = Sheet.GetEndRowIndex(ColumnIndex.Sequential, ColumnIndex.InLogic);
                for (var ri = RowIndex.Begin; ri <= endRowIndex; ri++)
                {
                    IRange row = Sheet.Range[ri, ColumnIndex.Sequential, ri, ColumnIndex.InLogic];

                    if (IsVariableRow(ri))
                    {
                        Variables.Add(new VariableRecord(Variables.Count, row));
                    }
                    else
                    {
                        Values.Add(new ValueRecord(Variables.Count - 1, Values.Count, row));
                    }
                }

                return ExecutionResult.Succeeded;
            }
            catch (Exception e)
            {
                System.Diagnostics.Debug.WriteLine(e.StackTrace);
                System.Diagnostics.Debugger.Break();
                return ExecutionResult.Failed;
            }
        }

        /// <summary>
        /// 変数設定行であるか？
        /// </summary>
        /// <param name="rowIndex"></param>
        /// <returns>判定結果</returns>
        private bool IsVariableRow(int rowIndex) =>
            !string.IsNullOrWhiteSpace(Sheet.GetValue(rowIndex, ColumnIndex.VarDelete)) ||
            !string.IsNullOrWhiteSpace(Sheet.GetValue(rowIndex, ColumnIndex.InVarName)) ||
            !string.IsNullOrWhiteSpace(Sheet.GetValue(rowIndex, ColumnIndex.OutVarName)) ||
            !string.IsNullOrWhiteSpace(Sheet.GetValue(rowIndex, ColumnIndex.Class)) ||
            !string.IsNullOrWhiteSpace(Sheet.GetValue(rowIndex, ColumnIndex.Type));

        #endregion

        #region 11. Structs ------------------------------------------------------------------------

        /// <summary>
        /// 列インデックス
        /// </summary>
        public struct ColumnIndex
        {
            /// <summary>通し番号列</summary>
            public const int Sequential = 0;
            /// <summary>不要変数フラグ列</summary>
            public const int VarDelete = 1;
            /// <summary>入力変数名称列</summary>
            public const int InVarName = 2;
            /// <summary>出力変数名称列</summary>
            public const int OutVarName = 3;
            /// <summary>分類列</summary>
            public const int Class = 4;
            /// <summary>型列</summary>
            public const int Type = 5;
            /// <summary>幅列</summary>
            public const int Width = 6;
            /// <summary>小数桁列</summary>
            public const int DecDigit = 7;
            /// <summary>変数ラベル列</summary>
            public const int VarLabel = 8;
            /// <summary>値除去フラグ列</summary>
            public const int ValDelete = 9;
            /// <summary>値</summary>
            public const int OutValue = 10;
            /// <summary>値ラベル</summary>
            public const int ValLabel = 11;
            /// <summary>中央値下限値列</summary>
            public const int LowerLimit = 12;
            /// <summary>中央値上限値列</summary>
            public const int UpperLimit = 13;
            /// <summary>変換対象値列</summary>
            public const int InValue = 14;
            /// <summary>変換ロジック列</summary>
            public const int InLogic = 15;
        }

        /// <summary>
        /// 行インデックス
        /// </summary>
        private struct RowIndex
        {
            /// <summary>入力開始行</summary>
            public const int Begin = 6;
        }

        #endregion
    }
}
