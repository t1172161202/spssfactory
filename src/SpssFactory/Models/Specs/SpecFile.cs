﻿namespace SpssFactory.Models.Specs
{
    using System.Collections.Generic;
    using SpreadsheetGear;
    using SpssFactory.Owns;

    /// <summary>
    /// ファイル
    /// </summary>
    public class SpecFile
    {
        #region 01. Fields -------------------------------------------------------------------------

        /// <summary>
        /// 仕様書ファイル拡張子
        /// </summary>
        public static readonly string FileExtension = ".xlsx";

        /// <summary>
        /// 仕様書ファイル選択ダイアログタイトル
        /// </summary>
        public static readonly string SpecFileDialogTitle = "仕様書ファイル選択";

        /// <summary>
        /// 仕様書ファイル選択ダイアログフィルタ
        /// </summary>
        public static readonly string SpecFileDialogFilter = "仕様書ファイル (*.xlsx)|*.xlsx";


        private IWorkbook book;

        #endregion

        #region 02. Constructors -------------------------------------------------------------------        

        /// <summary>
        /// 設定を初期化する
        /// </summary>
        public SpecFile(string path)
        {
            Path = path;
            Sheets = new List<SpecSheetBASE>()
            { 
                new SpecClassSheet(),
                new OrderSpecSheet(),
                new ItemSpecSheet(),
                new LogicSpecSheet(),
                new SystemSpecSheet(),
            };
        }

        #endregion

        #region 08. Properties ---------------------------------------------------------------------

        /// <summary>
        /// インポートパス
        /// </summary>
        public string Path { get; set; }

        /// <summary>
        /// シートリスト
        /// </summary>
        public List<SpecSheetBASE> Sheets { get; private set; }

        #endregion

        #region 10. Methods ------------------------------------------------------------------------

        /// <summary>
        /// ブックを開く
        /// </summary>
        /// <returns>接続成否</returns>
        public bool Open()
        {
            try
            {
                Close();
                book = Factory.GetWorkbook(Path);
                return ExecutionResult.Succeeded;
            }
            catch
            {
                return ExecutionResult.Failed;
            }
        }

        /// <summary>
        /// ワークシートオブジェクトを取得する
        /// </summary>
        /// <param name="name">シート名</param>
        /// <returns>取得成否</returns>
        public bool SetWorksheets()
        {
            try
            {
                Sheets.ForEach(s => s.Sheet = book.Worksheets[s.Name]);
                return ExecutionResult.Succeeded;
            }
            catch
            {
                return ExecutionResult.Failed;
            }
        }

        /// <summary>
        /// シート仕様を取得する
        /// </summary>
        /// <param name="kind"></param>
        /// <returns></returns>
        public SpecSheetBASE GetSheetSpec(SpecSheetKind kind) => Sheets.Find(s => s.Kind == kind);

        /// <summary>
        /// ブックを閉じる
        /// </summary>
        public void Close() => book?.Close();

        #endregion
    }
}