﻿namespace SpssFactory.Models.Specs
{
    using System;
    using System.Collections.Generic;
    using System.Text;
    using GongSolutions.Wpf.DragDrop;
    using Reactive.Bindings;
    using SpssFactory.Owns;
    using System.Data;
    using System.Linq;
    using System.Reactive.Linq;
    using System.Windows;
    using SpssFactory.Models;
    using SpssFactory.Helpers;
    using SpreadsheetGear;

    /// <summary>
    /// スペックファイル検証器
    /// </summary>
    public class SpecFileVerifier
    {
        #region 01. Fields -------------------------------------------------------------------------

        private static readonly string NotOneFileError = "複数のファイルが選択されました";
        private static readonly string InvalidFormatError = "不正なファイルが選択されました";
        private static readonly string NotOpenableError = "ファイルを開けませんでした";
        private static readonly string InvalidVersionError = "ファイルバージョンが不正です";

        #endregion

        #region 10. Methods ------------------------------------------------------------------------

        /// <summary>
        /// 検証をおこない、エラーが無ければ nullを、エラーがあれば詳細メッセージを返却する
        /// </summary>
        /// <param name="paths">検証ファイルパスリスト</param>
        /// <returns>エラーメッセージ</returns>
        public static string Invoke(List<string> paths)
        {
            SpecFile file = null;
            try
            {
                if (paths.Count > 1)
                {
                    return NotOneFileError;
                }

                // ファイル拡張子をチェックして、余計なファイルは除去する
                paths = paths.FindAll(t => t.ToLower().EndsWith(SpecFile.FileExtension));
                if (!paths.Any())
                {
                    // 拡張子不正
                    return InvalidFormatError;
                }

                file = new SpecFile(paths.First());
                if (ExecutionResult.Failed == file.Open())
                {
                    // ファイルオープン失敗
                    return NotOpenableError;
                }

                if (ExecutionResult.Failed == file.SetWorksheets())
                {
                    // 必要な設定シートがない不正なファイル
                    return InvalidFormatError;
                }

                // システム設定シートからの設定読み込み
                {
                    var ws = file.GetSheetSpec(SpecSheetKind.System);
                    if (ws.Read() == ExecutionResult.Failed || !(ws as SystemSpecSheet).IsValidKey())
                    {
                        return InvalidFormatError;
                    }

                    if (!(ws as SystemSpecSheet).IsValidVersion())
                    {
                        return InvalidVersionError;
                    }
                }

                // アイテム設定シートからの設定読み込み
                {
                    var ws = file.GetSheetSpec(SpecSheetKind.Item);
                    if (ws.Read() == ExecutionResult.Failed)
                    {
                        return InvalidFormatError;
                    }
                }


                // TODO: 各シートからの設定読み込み



                Globals.Spec = file;
                return null;
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                file.Close();
            }
        }
        
        #endregion
    }
}
