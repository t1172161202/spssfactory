﻿namespace SpssFactory.Models.Specs
{
    using CommonLib;
    using SpssFactory.Owns;

    /// <summary>
    /// システム設定シート
    /// </summary>
    public class SystemSpecSheet : SpecSheetBASE
    {
        #region 01. Fields -------------------------------------------------------------------------

        /// <summary>
        /// シート種別
        /// </summary>
        private static readonly SpecSheetKind kind = SpecSheetKind.System;

        #endregion

        #region 02. Constructors -------------------------------------------------------------------

        /// <summary>
        /// 基底クラスにシート種別を登録する
        /// </summary>
        public SystemSpecSheet() : base(kind) { }

        #endregion

        #region 08. Properties ---------------------------------------------------------------------  
        
        /// <summary>
        /// 仕様書キー
        /// </summary>
        public string Key { get; set; }

        /// <summary>
        /// 仕様書バージョン
        /// </summary>
        public string Version { get; set; }

        #endregion

        #region 10. Methods ------------------------------------------------------------------------

        /// <summary>
        /// 仕様を読み込む
        /// </summary>
        /// <returns>実行成否</returns>
        public override bool Read()
        {
            try
            {
                Key = Sheet.GetValue(CellAddress.Key);
                Version = Sheet.GetValue(CellAddress.Version);
                return ExecutionResult.Succeeded;
            }
            catch
            {
                return ExecutionResult.Failed;
            }
        }

        /// <summary>
        /// 仕様書ファイルキーは有効か？
        /// </summary>
        /// <returns>検証結果</returns>
        public bool IsValidKey() => Key == Properties.Resources.SpecFileKey;

        /// <summary>
        /// 仕様書ファイルバージョンは有効か？
        /// </summary>
        /// <returns></returns>
        public bool IsValidVersion() => Version == Properties.Resources.SpecFileVersion;

        #endregion

        #region 11. Structs ------------------------------------------------------------------------

        /// <summary>
        /// セルアドレス
        /// </summary>
        private struct CellAddress
        {
            /// <summary>
            /// キー
            /// </summary>
            public const string Key = "C2";

            /// <summary>
            /// バージョン
            /// </summary>
            public const string Version = "C3";
        }

        #endregion
    }
}
