﻿namespace SpssFactory.Models.Specs
{
    using SpssFactory.Owns;

    /// <summary>
    /// 出力（有無・順序）設定シート仕様
    /// </summary>
    public class OrderSpecSheet : SpecSheetBASE
    {
        #region 01. Fields -------------------------------------------------------------------------

        /// <summary>
        /// シート種別
        /// </summary>
        private static readonly SpecSheetKind kind = SpecSheetKind.Order;

        #endregion

        #region 02. Constructors -------------------------------------------------------------------

        /// <summary>
        /// 基底クラスにシート種別を登録する
        /// </summary>
        public OrderSpecSheet() : base(kind) { }

        #endregion
    }
}