﻿namespace SpssFactory.Models.Specs
{
    using System;
    using SpreadsheetGear;
    using SpssFactory.Helpers;
    using SpssFactory.Owns;

    /// <summary>
    /// シート仕様基底クラス
    /// </summary>
    public class SpecSheetBASE
    {
        #region 02. Constructors -------------------------------------------------------------------

        /// <summary>
        /// シート種別を登録する
        /// </summary>
        /// <param name="kind">シート種別</param>
        public SpecSheetBASE(SpecSheetKind kind) => Kind = kind;

        #endregion

        #region 08. Properties ---------------------------------------------------------------------

        /// <summary>
        /// シート種別
        /// </summary>
        public SpecSheetKind Kind { get; private set; }

        /// <summary>
        /// シート名
        /// </summary>
        public string Name { get { return Kind.GetValue(); } }

        /// <summary>
        /// ワークシートオブジェクト
        /// </summary>
        public IWorksheet Sheet { get; set; }

        #endregion

        #region 10. Methods ------------------------------------------------------------------------

        /// <summary>
        /// 仕様を読み込む
        /// </summary>
        /// <returns>実行成否</returns>
        public virtual bool Read() => throw new NotImplementedException();

        #endregion
    }
}
