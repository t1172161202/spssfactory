﻿namespace SpssFactory.ViewModels
{
    using GongSolutions.Wpf.DragDrop;
    using Reactive.Bindings;
    using SpssFactory.Helpers;
    using SpssFactory.Models.Specs;
    using SpssFactory.Owns;
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.Linq;
    using System.Reactive.Linq;
    using System.Threading.Tasks;
    using System.Windows;

    /// <summary>
    /// スプラッシュビューモデル
    /// </summary>
    public class SplashViewModel : ViewModelBASE, IDropTarget
    {
        #region 02. Constructors -------------------------------------------------------------------

        /// <summary>
        /// コマンドやアクションの登録をおこなう
        /// </summary>
        /// <param name="viewId">ビュー識別子</param>
        public SplashViewModel(ViewId viewId) : base(viewId)
        {
            // コマンドの登録
            {
                // ファイル選択パネルクリックコマンド登録
                FileSelectPanelClickCommand.Subscribe(e => OnFileSelectPanelClicked(e));
            }

            // 初期化アクションを登録
            InitializeAction = () => { /* なし */ };
        }

        #endregion

        #region 08. Properties (Commands) ----------------------------------------------------------

        /// <summary>
        /// ファイル選択パネルクリックコマンド
        /// </summary>
        public ReactiveCommand<RoutedEventArgs> FileSelectPanelClickCommand { get; } = new ReactiveCommand<RoutedEventArgs>();

        #endregion

        #region 10. Methods ------------------------------------------------------------------------

        /// <summary>
        /// ファイル選択パネルのドラッグ操作ステータスを更新する
        /// </summary>
        /// <param name="dropInfo">ドラッグ操作情報</param>
        public void DragOver(IDropInfo dropInfo)
        {
            try { dropInfo.Effects = DragDropEffects.Copy; }
            catch { }
        }

        /// <summary>
        /// ドロップ操作を処理する
        /// </summary>
        /// <param name="dropInfo">ドラッグ操作情報</param>
        public void Drop(IDropInfo dropInfo)
        {
            // ドラッグ＆ドロップで失っていたフォーカスをアプリに戻す
            Application.Current.MainWindow.Activate();

            // ドラッグ操作情報からドロップされたファイルパスリストを作成する
            var droppedPaths = ((DataObject)dropInfo.Data).GetFileDropList().Cast<string>().OrderBy(t => t).ToList();

            // 選択されたファイルの検証をおこなう
            VerifySelectedFiles(droppedPaths);
        }

        /// <summary>
        /// ファイル選択ダイアログを開き、選択されたファイルの検証処理を呼び出す
        /// </summary>
        /// <param name="parameter">コマンドパラメータ</param>
        private void OnFileSelectPanelClicked(object _)
        {
            // ファイル選択ダイアログを表示する
            WpfHelper.FileSelectDialogResult result =
                WpfHelper.ShowFileSelectDialog(
                    new Microsoft.Win32.OpenFileDialog()
                    {
                        Title = SpecFile.SpecFileDialogTitle,
                        Filter = SpecFile.SpecFileDialogFilter,
                        Multiselect = false,
                    });

            // ユーザによりファイルが選択されなかった場合は以下の処理をスキップ
            if (!result.HasSelected)
            {
                return;
            }

            // 選択されたファイルの検証をおこなう
            VerifySelectedFiles(result.SelectedPaths);
        }

        /// <summary>
        /// 選択されたファイルの検証をおこない、検証結果に応じた処理を呼び出す
        /// エラーがあれば通知、なければファイルを読み込んでホーム画面へ遷移する
        /// </summary>
        /// <param name="paths"></param>
        private void VerifySelectedFiles(List<string> paths)
        {
            Globals.MainWM.UpdateWindowStatus(WindowStatus.Running);
            Async();

            async void Async()
            {
                // 検証をおこない結果を取得する
                var errorMessage = await Task.Run(() => SpecFileVerifier.Invoke(paths));

                // エラーがあれば通知して終了
                if (!string.IsNullOrWhiteSpace(errorMessage))
                {
                    Globals.MainWM.UpdateWindowStatus(WindowStatus.Idle);
                    Globals.SendToSnackbar(errorMessage);
                    return;
                }
                // ホーム画面へ遷移する
                Globals.MainWM.WindowState.Value = WindowState.Maximized;
                Globals.MainWM.Transition(ViewId.Splash, ViewId.Home);                
            }
        }

        #endregion
    }
}
