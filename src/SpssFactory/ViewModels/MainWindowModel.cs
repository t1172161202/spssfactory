﻿namespace SpssFactory.ViewModels
{
    using MaterialDesignThemes.Wpf;
    using MaterialDesignThemes.Wpf.Transitions;
    using Reactive.Bindings;
    using Reactive.Bindings.Extensions;
    using SpssFactory.Owns;
    using SpssFactory.Views;
    using SpssFactory.WpfUtils;
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.Reactive.Linq;
    using System.Threading.Tasks;
    using System.Windows;
    using System.Windows.Input;
    using CI = WpfUtils.CommandImplementation;

    /// <summary>
    /// メインウィンドウモデル
    /// </summary>
    public class MainWindowModel : ViewModelBASE, IClosing
    {
        #region 02. Constructors -------------------------------------------------------------------

        /// <summary>
        /// コンストラクタ
        /// </summary>
        public MainWindowModel()
        {
            // コマンドの登録
            {
                // ウィンドウ起動完了コマンド登録
                WindowOpenedCommand = new ReactiveProperty<ICommand>(new CI(OnOpened));
            }

            // バージョン情報を設定
            {
                var assembly = System.Reflection.Assembly.GetExecutingAssembly().GetName();
                var ver = assembly.Version;
                Version.Value = $"Version {ver.Major}.{ver.Minor}.{ver.Build}";
            }
        }

        #endregion

        #region 08. Properties (Commands) ----------------------------------------------------------

        /// <summary>
        /// ウィンドウ起動完了後実行コマンド
        /// </summary>
        /// <remarks>
        /// 起動時に一回だけ使用され、スプラッシュ画面表示後に、ホーム画面への遷移処理を実行します
        /// </remarks>
        public ReactiveProperty<ICommand> WindowOpenedCommand { get; }

        #endregion
        #region 08. Properties (Reactive Properties) -----------------------------------------------

        /// <summary>
        /// アクティブビュー識別子
        /// </summary>
        /// <summary>   
        /// 初期値は起動時に表示するビュー識別子
        /// </summary>
        public ReactiveProperty<ViewId> ActiveViewId { get; } = new ReactiveProperty<ViewId>(ViewId.Splash);

        /// <summary>
        /// ウィンドウクローズボタンを表示するか？
        /// </summary>
        public ReactiveProperty<bool> WindowCloseButtonShows { get; } = new ReactiveProperty<bool>(true);

        /// <summary>
        /// ダイアログ表示ステータスフラグ
        /// </summary>
        public ReactiveProperty<bool> IsDialogOpen { get; } = new ReactiveProperty<bool>(false);

        /// <summary>
        /// プログレスバーが動作中か？
        /// </summary>
        public ReactiveProperty<bool> IsProgressBarIndeterminate { get; } = new ReactiveProperty<bool>(false);

        /// <summary>
        /// プログレスバー進捗値
        /// </summary>
        public ReactiveProperty<double> ProgressBarValue { get; } = new ReactiveProperty<double>(0);

        /// <summary>
        /// 処理経過時間
        /// </summary>
        public ReactiveProperty<string> ElapsedTime { get; } = new ReactiveProperty<string>(string.Empty);

        /// <summary>
        /// バージョン情報
        /// </summary>
        public ReactiveProperty<string> Version { get; } = new ReactiveProperty<string>(string.Empty);

        /// <summary>
        /// ウィンドウクローズ可否ステータス
        /// </summary>
        public ReactiveProperty<bool> Closable { get; set; } = new ReactiveProperty<bool>(true);

        /// <summary>
        /// ウィンドウステート
        /// </summary>
        public ReactiveProperty<WindowState> WindowState { get; set; } = new ReactiveProperty<WindowState>(System.Windows.WindowState.Normal);

        #endregion
        #region 08. Properties ---------------------------------------------------------------------

        /// <summary>
        /// トランジショナー
        /// </summary>
        public Transitioner ViewTransitioner { get; set; } = null;

        /// <summary>
        /// スナックバー
        /// </summary>
        public Snackbar Snackbar { get; set; } = null;

        #endregion

        #region 10. Methods ------------------------------------------------------------------------

        /// <summary>
        /// NOTE: アプリケーションで使用するスライドビューを登録する（トップレイヤー）
        /// </summary>
        /// <remarks>
        /// ビューの登録方法
        ///     1. ViewId, ViewModel, View を用意する
        ///     2. スライドリストに Viewを登録する
        /// </remarks>
        public void SetupSlides()
        {
            Views = new List<ViewBASE>
            {
                new SplashView(new SplashViewModel(ViewId.Splash)),
                new HomeView(new HomeViewModel(ViewId.Home)),
                new DemoView(new DemoViewModel(ViewId.Demo)),
            };
            Views.ForEach(v => ViewTransitioner.Items.Add(v));
        }

        /// <summary>
        /// メトロウィンドウクロージングイベントハンドラ
        /// </summary>
        /// <returns>ウィンドウクロージングイベントをキャンセルするか？</returns>
        public bool Closing() => !Closable.Value;

        /// <summary>
        /// ビューの遷移処理を実行する
        /// </summary>
        /// <param name="srcId">遷移元ビュー識別子</param>
        /// <param name="dstId">遷移先ビュー識別子</param>
        public void Transition(ViewId _, ViewId dstId)
        {
            if (ActiveViewId.Value == dstId)
            {
                return;
            }

            UpdateWindowStatus(WindowStatus.Running);
            Async(Views.Find(view => view.Id == ActiveViewId.Value), Views.Find(view => view.Id == dstId));

            async void Async(ViewBASE srcView, ViewBASE dstView)
            {
                await Task.Run(() => dstView.ViewModel.Initialize());
                UpdateWindowStatus(WindowStatus.Idle);
                srcView.BackwardWipe.Wipe(srcView, dstView, new Point(0.5, 0.5), ViewTransitioner);
                ActiveViewId.Value = dstView.Id;
            }
        }

        /// <summary>
        /// ウィンドウステータスを更新する
        /// </summary>
        /// <param name="windowStatus">ウィンドウステータス</param>
        public void UpdateWindowStatus(bool windowStatus)
        {
            Closable.Value = windowStatus == WindowStatus.Idle;
            IsDialogOpen.Value = windowStatus == WindowStatus.Running;
            IsProgressBarIndeterminate.Value = windowStatus == WindowStatus.Running;
        }

        /// <summary>
        /// ウィンドウ起動完了後に必要な処理を実行する
        /// </summary>
        /// <param name="parameter">コマンドパラメータ</param>
        private void OnOpened(object _)
        {
            Views.Find(view => view.Id == ViewId.Splash).ViewModel.Initialize();
            Globals.SendToSnackbar("ようこそ！");
        }

        #endregion
    }
}
