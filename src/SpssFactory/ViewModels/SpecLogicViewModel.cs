﻿namespace SpssFactory.ViewModels
{
    using SpssFactory.Owns;

    /// <summary>
    /// デモビューモデル
    /// </summary>
    public class SpecLogicViewModel : ViewModelBASE
    {
        #region 02. Constructors -------------------------------------------------------------------

        /// <summary>
        /// コマンドやアクションの登録をおこなう
        /// </summary>
        /// <param name="viewId">ビュー識別子</param>
        public SpecLogicViewModel(ViewId viewId) : base(viewId)
        {
            // 初期化内容を登録
            InitializeAction = () =>
            {
                // System.Threading.Thread.Sleep(1000);
            };
        }

        #endregion

        #region 10. Methods ------------------------------------------------------------------------

        /// <summary>
        /// クリックされたボタンに与えられた処理を実行する
        /// </summary>
        /// <param name="buttonKind">ボタン種別</param>
        protected override void OnButtonClickStrategy(ButtonKind buttonKind)
        {
            switch (buttonKind)
            {
                case ButtonKind.Ok: Globals.MainWM.Transition(ViewId, ViewId.Home); break;
            }
        }

        #endregion
    }
}
