﻿namespace SpssFactory.ViewModels
{
    using CommonLib;
    using Reactive.Bindings;
    using SpssFactory.Models.Records;
    using SpssFactory.Owns;
    using SpssFactory.Views;
    using System;
    using System.Collections.Generic;
    using System.Windows.Input;
    using WpfUtils;

    /// <summary>
    /// ビューモデル基底クラス
    /// </summary>
    public class ViewModelBASE : ObservableObject
    {
        #region 02. Constructors -------------------------------------------------------------------

        /// <summary>
        /// メインウィンドウのビューモデルから使用されるコンストラクタ
        /// </summary>
        public ViewModelBASE() { }

        /// <summary>
        /// ビューのビューモデルから使用されるコンストラクタ
        /// </summary>
        /// <param name="viewId">ビュー識別子</param>
        public ViewModelBASE(ViewId viewId)
        {
            ViewId = viewId;
            ButtonClickCommand = new ReactiveProperty<ICommand>(new CommandImplementation(OnButtonClick, CanButtonClick));
        }

        #endregion

        #region 08. Properties (Commands) ----------------------------------------------------------

        /// <summary>
        /// ボタンクリックコマンド
        /// </summary>
        public ReactiveProperty<ICommand> ButtonClickCommand { get; }

        #endregion
        #region 08. Properties ---------------------------------------------------------------------

        /// <summary>
        /// ビュー識別子
        /// </summary>
        public ViewId ViewId { get; private set; }

        /// <summary>
        /// ビューリスト
        /// </summary>
        public List<ViewBASE> Views { get; set; }

        /// <summary>
        /// 初期化アクション
        /// </summary>
        protected Action InitializeAction { get; set; }

        #endregion

        #region 10. Methods ------------------------------------------------------------------------

        /// <summary>
        /// 初期化アクションを実行する
        /// </summary>
        public void Initialize() => InitializeAction?.Invoke();

        /// <summary>
        /// ボタンクリック可否判定結果を返却する
        /// </summary>
        /// <param name="kind">ボタン種別</param>
        /// <returns>クリック可否</returns>
        protected virtual bool CanButtonClickStrategy(ButtonKind kind) => true;

        /// <summary>
        /// ボタンクリック処理ストラテジー
        /// </summary>
        /// <param name="kind">ボタン種別</param>
        protected virtual void OnButtonClickStrategy(ButtonKind kind) => throw new NotImplementedException();

        /// <summary>
        /// ボタンクリック可否判定結果を取得する
        /// </summary>
        /// <param name="parameter">コマンドパラメータ</param>
        /// <returns>ボタンクリック可否</returns>
        private bool CanButtonClick(object parameter) =>
            CanButtonClickStrategy(ConvertHelper.ToEnum<ButtonKind>(parameter));

        /// <summary>
        /// クリックされたボタンに与えられた処理を実行する
        /// </summary>
        /// <param name="parameter">コマンドパラメータ</param>
        private void OnButtonClick(object parameter)
        {
            var values = (object[])parameter;

            // HACK: パラメータから取得したボタン情報ログ書き出し
            var viewId = ConvertHelper.ToEnum<ViewId>(values[0]);
            var buttonKind = ConvertHelper.ToEnum<ButtonKind>(values[1]);
            var buttonLabel = ConvertHelper.ToString(values[2]);


            // 固有のダイアログボタンクリック処理
            OnButtonClickStrategy(buttonKind);
        }

        #endregion
    }
}
