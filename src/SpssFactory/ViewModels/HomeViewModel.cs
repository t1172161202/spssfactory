﻿namespace SpssFactory.ViewModels
{
    using MaterialDesignThemes.Wpf.Transitions;
    using Reactive.Bindings;
    using SpssFactory.Owns;
    using SpssFactory.Views;
    using System.Collections.Generic;
    using System.Threading.Tasks;
    using System.Windows;

    /// <summary>
    /// ホームビューモデル
    /// </summary>
    public class HomeViewModel : ViewModelBASE
    {
        #region 02. Constructors -------------------------------------------------------------------

        /// <summary>
        /// コマンドやアクションの登録をおこなう
        /// </summary>
        /// <param name="viewId">ビュー識別子</param>
        public HomeViewModel(ViewId viewId) : base(viewId)
        {
            // 初期化内容を登録
            InitializeAction = () =>
            {
                Transition(ViewId.TBD, ViewId.SpecItem);
                System.Threading.Thread.Sleep(1000);
            };
        }

        #endregion

        #region 08. Properties (Reactive Properties) -----------------------------------------------

        /// <summary>
        /// アクティブビュー識別子
        /// </summary>
        /// <summary>   
        /// 初期値は起動時に表示するビュー識別子
        /// </summary>
        public ReactiveProperty<ViewId> ActiveViewId { get; } = new ReactiveProperty<ViewId>(ViewId.SpecItem);

        #endregion

        #region 08. Properties ---------------------------------------------------------------------

        /// <summary>
        /// トランジショナー
        /// </summary>
        public Transitioner ViewTransitioner { get; set; } = null;

        #endregion

        #region 10. Methods ------------------------------------------------------------------------

        /// <summary>
        /// NOTE: アプリケーションで使用するスライドビューを登録する（サブレイヤー）
        /// </summary>
        /// <remarks>
        /// ビューの登録方法
        ///     1. ViewId, ViewModel, View を用意する
        ///     2. スライドリストに Viewを登録する
        /// </remarks>
        public void SetupSlides()
        {
            Views = new List<ViewBASE>
            {
                new SpecOrderView(new SpecOrderViewModel(ViewId.SpecOrder)),
                new SpecItemView(new SpecItemViewModel(ViewId.SpecItem)),
                new SpecLogicView(new SpecLogicViewModel(ViewId.SpecLogic)),
                new SpecClassView(new SpecClassViewModel(ViewId.SpecClass)),
            };
            Views.ForEach(v => ViewTransitioner.Items.Add(v));
        }

        /// <summary>
        /// ビューの遷移処理を実行する
        /// </summary>
        /// <param name="srcId">遷移元ビュー識別子</param>
        /// <param name="dstId">遷移先ビュー識別子</param>
        public void Transition(ViewId srcId, ViewId dstId)
        {
            Globals.MainWM.UpdateWindowStatus(WindowStatus.Running);
            Async(Views.Find(view => view.Id == srcId), Views.Find(view => view.Id == dstId));

            async void Async(ViewBASE srcView, ViewBASE dstView)
            {
                await Task.Run(() => dstView.ViewModel.Initialize());
                Globals.MainWM.UpdateWindowStatus(WindowStatus.Idle);
                srcView?.BackwardWipe.Wipe(srcView, dstView, new Point(0.5, 0.5), ViewTransitioner);
                ActiveViewId.Value = dstView.Id;
            }
        }

        /// <summary>
        /// クリックされたボタンに与えられた処理を実行する
        /// </summary>
        /// <param name="buttonKind">ボタン種別</param>
        protected override void OnButtonClickStrategy(ButtonKind buttonKind)
        {
            ViewId dstId = ViewId.TBD;
            switch (buttonKind)
            {
                case ButtonKind.ToOrder: dstId = ViewId.SpecOrder; break;
                case ButtonKind.ToItem: dstId = ViewId.SpecItem; break;
                case ButtonKind.ToLogic: dstId = ViewId.SpecLogic; break;
                case ButtonKind.ToClass: dstId = ViewId.SpecClass; break;

                case ButtonKind.Ok: dstId = ViewId.Demo; break;
            }

            if (ActiveViewId.Value == dstId)
            {
                return;
            }

            Globals.HomeVM.Transition(ActiveViewId.Value, dstId);
        }

        #endregion
    }
}
