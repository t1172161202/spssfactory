﻿namespace SpssFactory.ViewModels
{
    using Reactive.Bindings;
    using Reactive.Bindings.Extensions;
    using SpssFactory.Helpers;
    using SpssFactory.Models.Records;
    using SpssFactory.Models.Specs;
    using SpssFactory.Owns;
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.Linq;
    using System.Reactive.Linq;

    /// <summary>
    /// アイテム一仕様ビューモデル
    /// </summary>
    public class SpecItemViewModel : ViewModelBASE
    {
        #region 02. Constructors -------------------------------------------------------------------

        /// <summary>
        /// コマンドやアクションの登録をおこなう
        /// </summary>
        /// <param name="viewId">ビュー識別子</param>
        public SpecItemViewModel(ViewId viewId) : base(viewId)
        {
            // 連鎖更新するプロパティを登録
            {
                // アイテム一覧選択行が更新されたら関連表示を更新する
                SelectedVariableIndex
                    .ObserveProperty(x => x.Value)
                        .Subscribe(x =>
                        {
                            // アイテム詳細画面表示を更新する
                            SelectedVariable.Value =
                                Variables.Value == null || Variables.Value.Count == 0
                                    ? null
                                    : Variables.Value[x];

                            // 値一覧表示を更新する
                            SelectedValues.Value =
                                Values.Value == null || Values.Value.Count == 0
                                    ? null
                                    : Values.Value.FindAll(t => t.ParentIndex == x);
                        });
            }

            // 画面用リストを初期化
            {
                ClassList.Value = new List<string>();
                for (char c = 'A'; c <= 'Z'; c++)
                {
                    ClassList.Value.Add(c.ToString());
                }

                TypeList.Value = new List<string>();
                foreach (TypeKind value in Enum.GetValues(typeof(TypeKind)))
                {
                    TypeList.Value.Add(value.GetValue());
                }
            }

            // 初期化内容を登録
            InitializeAction = () =>
            {
                // UIスレッドに処理を委任してレコードを更新
                System.Windows.Application.Current.Dispatcher.Invoke(delegate
                {
                    Variables.Value = new List<VariableRecord>();
                    ItemSpecSheet.Variables.ForEach(v => Variables.Value.Add(v));

                    Values.Value = new List<ValueRecord>();
                    ItemSpecSheet.Values.ForEach(v => Values.Value.Add(v));

                    SelectedVariableIndex.Value = ItemSpecSheet.Variables.Count > 0 ? 0 : -1;
                });
            };
        }

        #endregion

        #region 08. Properties (Reactive Properties) -----------------------------------------------

        /// <summary>
        /// 変数リスト
        /// </summary>
        public ReactiveProperty<List<VariableRecord>> Variables { get; set; } = new ReactiveProperty<List<VariableRecord>>();

        /// <summary>
        /// 値リスト
        /// </summary>
        public ReactiveProperty<List<ValueRecord>> Values { get; set; } = new ReactiveProperty<List<ValueRecord>>();

        /// <summary>
        /// 選択された（変数の）値リスト
        /// </summary>
        public ReactiveProperty<List<ValueRecord>> SelectedValues { get; set; } = new ReactiveProperty<List<ValueRecord>>();

        /// <summary>
        /// 選択された変数インデックス
        /// </summary>
        public ReactiveProperty<int> SelectedVariableIndex { get; set; } = new ReactiveProperty<int>(-1);

        /// <summary>
        /// 選択された変数
        /// </summary>
        public ReactiveProperty<VariableRecord> SelectedVariable { get; set; } = new ReactiveProperty<VariableRecord>();

        /// <summary>
        /// 分類リスト（変数詳細画面用）
        /// </summary>
        public ReactiveProperty<List<string>> ClassList { get; set; } = new ReactiveProperty<List<string>>();

        /// <summary>
        /// 型リスト（変数詳細画面用）
        /// </summary>
        public ReactiveProperty<List<string>> TypeList { get; set; } = new ReactiveProperty<List<string>>();

        #endregion
        #region 08. Properties ---------------------------------------------------------------------


        #endregion

        #region 10. Methods ------------------------------------------------------------------------

        /// <summary>
        /// クリックされたボタンに与えられた処理を実行する
        /// </summary>
        /// <param name="buttonKind">ボタン種別</param>
        protected override void OnButtonClickStrategy(ButtonKind buttonKind)
        {
            switch (buttonKind)
            {
                case ButtonKind.Ok: Globals.MainWM.Transition(ViewId, ViewId.Demo); break;
            }
        }

        #endregion
    }
}
