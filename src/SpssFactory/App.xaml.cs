﻿namespace SpssFactory
{
    using SpssFactory.Owns;
    using SpssFactory.Views;
    using System;
    using System.Windows;
    using System.Windows.Controls;
    using System.Windows.Threading;    

    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {
        #region 01. Fields -------------------------------------------------------------------------
        #endregion

        #region 02. Constructors -------------------------------------------------------------------

        /// <summary>
        /// アプリケーションに必要な初期化
        /// </summary>
        public App()
        {
            // 未処理例外を処理する
            DispatcherUnhandledException += (object sender, DispatcherUnhandledExceptionEventArgs e) =>
            {
                /*
                // 未処理の例外に関するメッセージを取得する
                var detail = e.Exception.Message == string.Empty
                    ? $"\n({e.GetType()})"
                    : $"\n({e.Exception.Message.GetType()} ({e.Exception.Message}))";
                var message = string.Format(D2.Properties.Resources.ME0001, detail);

                // 未処理の例外に関するメッセージを表示する
                MessageBox.Show(message);

                // 未処理の例外に関するログを出力
                logger.Fatal(message.RemoveCrLf());
                */

                // 未処理の例外の処理を終了とする
                e.Handled = true;

            };
            
            // UIスレッド以外で発生した未処理例外を処理
            AppDomain.CurrentDomain.UnhandledException += (object sender, UnhandledExceptionEventArgs e) =>
            {
                // UNDONE: ここを通るケースを検証し、しかるべき異常停止処理を追加すること！
                Shutdown();
            };

            // ツールチップ設定
            {
                // ツールチップを出しっぱなしにする
                ToolTipService.ShowDurationProperty.OverrideMetadata(
                    typeof(DependencyObject),
                    new FrameworkPropertyMetadata(int.MaxValue));

                // ツールチップが設定されているコントロールが Disabledでもツールチップを表示する
                ToolTipService.ShowOnDisabledProperty.OverrideMetadata(
                    typeof(Control),
                    new FrameworkPropertyMetadata(true));
            }
        }

        #endregion

        #region 10. Methods ------------------------------------------------------------------------

        /// <summary>
        /// アプリケーション起動イベントハンドラ
        /// </summary>
        /// <param name="sender">イベント発生元オブジェクト</param>
        /// <param name="e">イベント詳細情報</param>
        private void OnApplicationStartup(object sender, StartupEventArgs e)
        {
            var window = new MainWindow();
            window.Show();
        }

        /// <summary>
        /// アプリケーション終了イベントハンドラ
        /// </summary>
        /// <param name="sender">イベント発生元オブジェクト</param>
        /// <param name="e">イベント詳細情報</param>
        private void OnApplicationExit(object sender, ExitEventArgs e) { }

        #endregion
    }
}
